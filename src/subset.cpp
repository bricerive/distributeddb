//
//  subset.cpp
//  distributed
//
//  Created by Brice Rivé on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "subset.h"
#include "transfer.h"
#include "utils.h"
#include "tinyxml/tinyxml.h"
#include "mylib/idioms.h"
#include "mylib/log.h"
#include "mylib/error.h"
#include "mylib/sha1.h"
#include "mylib/config.h"
#include <boost/foreach.hpp>
#include <tbb/pipeline.h>
#include <tbb/parallel_for.h>
#include <tbb/spin_mutex.h>

using namespace mylib;
using namespace std;
using namespace boost::filesystem;
using namespace tbb;

static const char *subsetFileName = " subset.xml";

#define USE_TBB 1
static spin_mutex gMutex;

Subset::Subset(const Path &path)
    :mPath(path)
{
}

template<typename T>
static TiXmlElement *AddTextElt(TiXmlElement *parent, const T &nameValue)
{
    TiXmlElement *elt = new TiXmlElement(nameValue.Name());  
    parent->LinkEndChild(elt);
    elt->LinkEndChild(new TiXmlText(to_string<typename T::Type>(nameValue)));
    return elt;
}

static TiXmlElement *AddElt(TiXmlElement *parent, const std::string &name)
{
    TiXmlElement *elt = new TiXmlElement(name);  
    parent->LinkEndChild(elt);
    return elt;
}

static const string kStrSubset("subset");
static const string kStrInfo("info");
static const string kStrEntries("entries");
static const string kStrUnwanted("unwanted");

// for sorting by name
struct SubsetInfo {
    SubsetInfo(const string &crc, const string &location, const string &fileName, int size)
    : crc(crc), location(location), fileName(fileName), size(size) {}
    NvString(crc);
    NvString(location);
    NvString(fileName);
    NvInt(size);
    bool operator<(const SubsetInfo &rhs)const { return fileName < rhs.fileName; }
};

void Subset::Save(const Path &xmlPath)
{
    LogF(LOG_TRACE, "Subset::Save(%s)", xmlPath.c_str());

    // build the TinyXml DOM
    TiXmlDocument doc;
	doc.LinkEndChild(new TiXmlDeclaration("1.0","","")); 
    TiXmlElement *subsetElt = new TiXmlElement(kStrSubset);  
	doc.LinkEndChild(subsetElt);
    
    // Save the info
    const SubsetInfoNv &info(mInfo);
    TiXmlElement *infoElt = AddElt(subsetElt, kStrInfo);
    AddTextElt(infoElt, info.set);
    AddTextElt(infoElt, info.subset);
    AddTextElt(infoElt, info.minSize);
    AddTextElt(infoElt, info.updateFromOthers);
    AddTextElt(infoElt, info.updateOthers);
    
    // Save the entries
    TiXmlElement *entriesElt = AddElt(subsetElt, kStrEntries);
    const SubsetMap &map(mMap);
    
    // Sort the entries by name
    vector<SubsetInfo> sorted;
    BOOST_FOREACH(const SubsetMap::value_type &v, map) {
        const SubsetEntry &subsetEntry(v.second);
        sorted.push_back(SubsetInfo(v.first, subsetEntry.location, subsetEntry.fileName, subsetEntry.size));
    }
    sort(sorted.begin(), sorted.end());
                         
    BOOST_FOREACH(const SubsetInfo &v, sorted) {
        TiXmlElement *entry = AddElt(entriesElt, v.crc);
        AddTextElt(entry, v.location);
        AddTextElt(entry, v.fileName);
        AddTextElt(entry, v.size);
    }
    
    // Save the unwanted
    TiXmlElement *unwantedElt = AddElt(subsetElt, kStrUnwanted);
    BOOST_FOREACH(const string &crc, mUnwanted) {
        AddElt(unwantedElt, crc);
    }

    doc.SaveFile(xmlPath.c_str());  
}

template<typename T>
static TiXmlElement *XmlGetElement(T &parent, const string &name)
{
    TiXmlElement *elt = parent.FirstChildElement(name);
    if (elt==0)
        throw Err(_WHERE, "Subset xml file syntax error (missing %s)", name.c_str());
    return elt;
}

template<typename T>
static void LoadPropertyValue(TiXmlElement *parent, T &nameValue)
{
    TiXmlElement *elt = XmlGetElement(*parent, nameValue.Name());
    const char *str = elt->GetText();
    if (str==0)
        throw Err(_WHERE, "Subset xml file syntax error (missing text %s)", nameValue.Name().c_str());
    nameValue = from_string<typename T::Type>(str);
}

template<typename T>
static void LoadPropertyValue(TiXmlElement *parent, T &nameValue, const typename T::Type &defaultValue)
{
    TiXmlElement *elt = parent->FirstChildElement(nameValue.Name());
    nameValue = defaultValue;
    if (elt==0)
        return;
    const char *str = elt->GetText();
    if (str==0)
        return;
    nameValue = from_string<typename T::Type>(str);
}

void Subset::Load(const Path &xmlPath, bool skipInfo)
{
    LogF(LOG_TRACE, "Subset::Load(%s)", xmlPath.c_str());

    TiXmlDocument doc(xmlPath.c_str());
	if (!doc.LoadFile()) throw Err(_WHERE, "Cannot load xml file [%s]", xmlPath.c_str());
    
    TiXmlElement *subsetElt = XmlGetElement(doc, kStrSubset);
    TiXmlElement *infoElt = XmlGetElement(*subsetElt, kStrInfo);
    
    if (!skipInfo)
    {
        SubsetInfoNv &info(mInfo);
        LoadPropertyValue(infoElt, info.set);
        LoadPropertyValue(infoElt, info.subset);
        LoadPropertyValue(infoElt, info.minSize);
        LoadPropertyValue(infoElt, info.updateFromOthers);
        LoadPropertyValue(infoElt, info.updateOthers);
        LoadPropertyValue(infoElt, info.forceRefreshCrcs, false);
    }
    
    TiXmlElement *entriesElt = subsetElt->FirstChildElement(kStrEntries);
    if (entriesElt)
    {
        for (TiXmlElement *pElem=entriesElt->FirstChildElement(); pElem; pElem=pElem->NextSiblingElement())
        {
            SubsetEntry entry;
            string crc=pElem->Value();
            
            LoadPropertyValue(pElem, entry.location, string());
            LoadPropertyValue(pElem, entry.fileName);
            LoadPropertyValue(pElem, entry.size);
            
            mMap[crc] = entry;
        }
    }
    TiXmlElement *unwantedElt = subsetElt->FirstChildElement(kStrUnwanted);
    if (unwantedElt)
    {
        for (TiXmlElement *pElem=unwantedElt->FirstChildElement(); pElem; pElem=pElem->NextSiblingElement())
        {
            mUnwanted.insert(pElem->Value());
        }
    }
}


void Subset::ComputeCrcs::operator()(const tbb::blocked_range<size_t>& r) const
{
    CONFIG_PARAM(int, "distributed", crcMaxSizeMb);
    for( size_t i=r.begin(); i!=r.end(); ++i )
    {
        const FileInfo &fileInfo(mFiles[i]);
        gMutex.lock();
        bool hasFile=mSubset.HasFile(fileInfo);
        gMutex.unlock();
        if (!hasFile)
        {
            size_t fileSize(fileInfo.mSize);
            Path crtPath(fileInfo.mPath);
            
            string physicalSize = to_string(fileSize);
            string result;
            CSHA1 encoder;
            // Get the full path
            string fullPath = crtPath.string();
            // Hash the file content
            string hashCode = to_string(encoder.EncodeFileWithSeed(fullPath, crcMaxSizeMb*1024*1024, static_cast<int>(fileSize)));
            LogF(LOG_TRACE, "ComputeCrcs: [%s/%s] %s", hashCode.c_str(), physicalSize.c_str(), crtPath.filename().c_str());
            
            path relativePath = mSubset.RelativePath(fullPath);
            string xmlKey = string("_")+hashCode; // xml key cannot start with a number
            gMutex.lock();
            SubsetMap::const_iterator i=mSubset.mMap.find(xmlKey);
            if (i!=mSubset.mMap.end()) {
                LogF(LOG_TRACE, "ComputeCrcs: Duplicate entry [%s/%s] %s", hashCode.c_str(), physicalSize.c_str(), relativePath.c_str());
                LogF(LOG_TRACE, "             Duplicate entry [%s/%s] %s", hashCode.c_str(), physicalSize.c_str(), i->second.RelativePath().c_str());
            } else
                mSubset.mMap[xmlKey] = SubsetEntry(relativePath.parent_path().string(), crtPath.filename().string(), fileSize);
            gMutex.unlock();
        }
    }
}

Subset::ComputeCrcs::ComputeCrcs(const Files &files, Subset &subset)
: mFiles(files), mSubset(subset)
{}

Path Subset::RelativePath(const Path &absolutePath)const
{
    return absolutePath.string().substr(GetPath().string().size()+1);
}

void Subset::UpdateCrcs(const Files &files)
{
#if USE_TBB
    parallel_for(blocked_range<size_t>(0,files.size(),1), ComputeCrcs(files, *this) );
#else
    ComputeCrcs(files, *this)(tbb::blocked_range<size_t>(0, files.size()));
#endif  
}

bool Subset::HasFile(const FileInfo &fileInfo)
{
    size_t fileSize(fileInfo.mSize);
    Path crtPath(fileInfo.mPath);
    string fullPath = crtPath.string();
    path relativePath = RelativePath(fullPath);
    SubsetEntry toFind(relativePath.parent_path().string(), crtPath.filename().string(), fileSize);
    for (SubsetMap::const_iterator i=mMap.begin(); i!=mMap.end(); ++i)
        if (i->second == toFind)
            return true;
    return false;
}

void Subset::ReadInfo()
{
    LogF(LOG_TRACE, "Subset::ReadInfo() [%s]", mPath.c_str());

    // look for subset specification file
    if (!is_directory(mPath))
        throw Err(_WHERE, "Not a subset (not a directory)");
    path subsetFile(mPath/subsetFileName);
    if (!exists(subsetFile))
        throw Err(_WHERE, "Not a subset (missing %s)", subsetFileName);
    
    Load(subsetFile);
    //LogF(LOG_TRACE, "ProcessItem: set=[%s] subset=[%s] minSize[%d]", set.c_str(), subset.c_str(), minSize);
}

void Subset::Refresh(const Path &xmlPath)
{
    LogF(LOG_TRACE, "Subset::Refresh(%s)", xmlPath.c_str());

    // explore the subset:
    // 1- Update the subset record
    // -for each entry
    // --if directory, recurse
    // --if file, store CRC, size, relative path    
    Files files;
    recursive_directory_iterator itr(mPath);
    recursive_directory_iterator end;
    for (; itr!=end; ++itr)
    {
        Path crtPath(itr->path());
        if (!exists(crtPath) || is_directory(crtPath))
            continue;
        string fileName = crtPath.filename().string();
        if (fileName == subsetFileName || fileName[0] == '.')
            continue;
        size_t fileSize = file_size(crtPath);
        if (fileSize < MinSize())
            continue;
        //LogF(LOG_TRACE, "-ProcessItem [%s]", crtPath.string().c_str());
        files.push_back(FileInfo(crtPath, fileSize));
    }
    
    if (!mInfo.forceRefreshCrcs && exists(xmlPath))
    { // only compute missing CRCs
        Unwanted tmp=mUnwanted; // The unwanted list from the " subset.xml" file overrules the one in the db
        Load(xmlPath, true); // skipInfo=true
        mUnwanted=tmp;
        
        for (SubsetMap::iterator i=mMap.begin(); i!=mMap.end(); )
        {
            const SubsetEntry &subsetEntry(i->second);
            path absolutePath = GetPath()/subsetEntry.RelativePath();
            if (!exists(absolutePath))
            {
                mMap.erase(i++); // invalidates i ?
            }
            else
            {
                ++i;
            }
            
        }
    }
    
    // todo - special token files: indicate that we don't have a specific file but we don't want it either
    
    
    // This is expensive so it is done by itself
    UpdateCrcs(files);
}

// 3- Update the transfer cache
// -for each other subset
// --for each file in our record
// ---if the subset does not have it
// ----add to list of subset
// --if list of subset not empty, copy to transfer cache with list of subsets
void Subset::UpdateCache(Transfer &transfer, const Path &setDir, const Path &xmlPath)
{
    LogF(LOG_TRACE, "Subset::UpdateCache(transfer, %s)", setDir.c_str());

    if (!UpdateOthers())
        return;
    const SubsetMap &subsetMap(mMap);
    
    CONFIG_PARAM(unsigned long, "distributed", cacheMaxSizeGb);
    unsigned long cacheMaxSizeBytes = cacheMaxSizeGb*1024*1024*1024;
    long transferSize = GetFolderSize(transfer.GetDir());
    
    // for each other subset
    directory_iterator end_itr;
    for (directory_iterator itr(setDir); itr!=end_itr; ++itr)
    {
        path crtPath = itr->path();
        string subsetExtension = crtPath.extension().string();
        if (subsetExtension == ".xml" )
        {
            string crtSubset = crtPath.stem().string();
            if (crtSubset != Name())
            {
                
                // load the other subset
                Subset otherSubset("");
                otherSubset.Load(crtPath);
                if (!otherSubset.UpdateFromOthers())
                    continue;
                const SubsetMap &otherSubsetMap(otherSubset.mMap);
                
                // for each entry in this subset
                SubsetMap::const_iterator end1=subsetMap.end();
                SubsetMap::const_iterator end2=otherSubsetMap.end();
                for (SubsetMap::const_iterator i=subsetMap.begin(); i!=end1; ++i)
                {
                    const string &crc(i->first);
                    // if the other subset does not have it
                    if (otherSubsetMap.find(crc) == end2 && !otherSubset.IsUnwanted(crc))
                    {
                        
                        if (transfer.HasEntry(crc))
                        { // If it is already in the cache, just add it to the destinations
                            TransferEntry &transferEntry(transfer.GetEntry(crc));
                            transferEntry.mDestinations.insert(crtSubset);
                            // save the transfer map everytime in case we break
                            transfer.Save(xmlPath);
                        }
                        else
                        { // otherwise try to add it to the cache
                            const SubsetEntry &subsetEntry(i->second);
                            int fileSize = subsetEntry.size;
                            
                            if (transferSize+fileSize < cacheMaxSizeBytes)
                            {
                                transferSize += fileSize;
                                // add it to the transfer
                                TransferEntry &transferEntry(transfer.GetEntry(crc));
                                transferEntry.mName = subsetEntry.fileName;
                                path filePath = transfer.GetDir()/string(subsetEntry.fileName);
                                transferEntry.mDestinations.insert(crtSubset);
                                if (!exists(filePath))
                                {
                                    LogF(LOG_TRACE, "UpdateCache: [%s] <- %s", otherSubset.Name().c_str(), string(subsetEntry.fileName).c_str());
                                    copy_file(GetPath()/subsetEntry.RelativePath(), filePath);
                                    
                                    // save the transfer map everytime in case we break
                                    transfer.Save(xmlPath);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

bool Subset::Wants(const std::string &crc)
{
    return mMap.find(crc) == mMap.end() && UpdateFromOthers();
}

bool Subset::AddEntry(const string &name, const string &crc, int size, const Path &src)
{
    // Get the disk free space
    long freeSpace = GetFreeDiskSpace(GetPath());
    // if we still don't have it and we want it
    if (Wants(crc))
    {
        // copy it over and add it to our map
        Path dstDir = GetPath()/"_DistributedDb";
        if (!exists(dstDir))
            create_directories(dstDir);

        Path dst = dstDir/name;
        if (!exists(dst))
            copy_file(src, dst);
        AddEntry(crc, SubsetEntry(dstDir.string(), name, size));
        return true;
    }
    return false;
}

void Subset::AddEntry(const std::string &crc, const SubsetEntry &entry)
{
    mMap[crc]= entry;
}

bool Subset::IsUnwanted(const std::string &crc)const
{
    return mUnwanted.find(crc) != mUnwanted.end();
}

