//
//  distributed.h
//  distributed
//
//  Created by Brice Rivé on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <boost/filesystem.hpp>
#include <string>
#include <map>
#include <set>
#include <iostream>
#include "subset.h"
#include "transfer.h"

class Distributed
{
public:
    typedef boost::filesystem::path Path;
    static void Process(Path subsetPath);
};