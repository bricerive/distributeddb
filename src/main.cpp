//
//  main.cpp
//  distributed
//
//  Created by Brice Rivé on 10/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include "distributed.h"
#include "mylib/error.h"
#include "mylib/log.h"
#include "mylib/configproviderptree.h"
#include "mylib/config.h"
#include "mylib/socket.h"
#include "tinyxml/tinyxml.h"

#include <boost/filesystem.hpp>
#include <tbb/task_scheduler_init.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace mylib;
using namespace mylib::config;
using namespace std;
using namespace boost;
using namespace boost::filesystem;
using namespace tbb;

int main (int argc, char * const argv[])
{
    // Get the location for config and log files
    // We use the location of the executable because that is the only thing 
    // we can rely on. For example when inside of a DropScript app, the 
    // current dir will be /
    // this means that config files must be copied to where the executable
    // is (in the build dir AND in the script dir).
    // Also means that debug runs should start in the build dir
    path mypath = system_complete( path( argv[0] ) ).branch_path();
    string exeName = strrchr(argv[0], '/')+1;
    
	// Setup config
    path configFilePath = mypath / (exeName + ".config");
    Provider *p = new ProviderXml<Recursive, Attributes>(configFilePath.c_str());
    TheProvider::AddProvider(p);
    
    // Setup logging
    path logFilePath = mypath / (exeName + "_log.txt");
	ofstream logFile(logFilePath.string().c_str());
	Log::AddStreamBuf(logFile.rdbuf());
    Log::SetLevel(ConfigParam<Log::LogType>("distributed", "logLevel"));
    
	LogF(LOG_TRACE, "################################################");
	LogF(LOG_TRACE, "###        NEW SESSION                       ###");
	LogF(LOG_TRACE, "################################################");
	LogF(LOG_TRACE, "Entering main");
	LogF(LOG_TRACE, "mypath: %s", mypath.string().c_str());
	LogF(LOG_TRACE, "Config file: %s", configFilePath.string().c_str());
	LogF(LOG_TRACE, "Log file: %s", logFilePath.string().c_str());

    // init tbb
    task_scheduler_init init;
    
    // Make sure TinyXml won't condense spaces à la HTML
    TiXmlBase::SetCondenseWhiteSpace(false);

    // Process each argument
    for (int i=1; i<argc; i++) {
        try {
            LogF(LOG_TRACE, "Process argument: [%s]", argv[i]);
            Distributed::Process(argv[i]);
        } catch(std::exception &e) {
            LogF(LOG_ERROR, "Item processing failed [%s] %s", argv[i], e.what());
        } catch(...) {
            LogF(LOG_ERROR, "Item processing failed [%s]", argv[i]);
        }
    }

    return 0;
}




