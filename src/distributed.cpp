//
//  distributed.cpp
//  distributed
//
//  Created by Brice Rivé on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include "distributed.h"
#include "utils.h"
#include "mylib/log.h"
using namespace boost::filesystem;
using namespace mylib;

// process our subset
void Distributed::Process(Path subsetPath)
{
    // create the subset object
    // Load up the info from the subset's XML description file
    Subset subset(subsetPath);
    subset.ReadInfo();

    // Set up the workdir
    Path workingDir(HomeDir()/"Library/Caches/com.bricerive.DistributedDb");
    if (!exists(workingDir))
        create_directory(workingDir);    
    Path setDir(workingDir/subset.Set());
    if (!exists(setDir))
        create_directory(setDir);
    Path transferDir(setDir/"transfer");
    if (!exists(transferDir))
        create_directory(transferDir);
    
    // Todo:
    // for the transfer dir: check consistency i.e:
    // any file that is not in the xml goes
    // any xml entry that has no file goes (check sizes too)

    
    // get the subset contents
    Path subsetDb(setDir/(subset.Name()+".xml"));
    subset.Refresh(subsetDb);
    
    // save the subset map
    // (not needed in theory but useful in case of error in the transfer update below)
    subset.Save(subsetDb);

    // get the transfer map
    Transfer transfer(transferDir);
    Path transferDb(transferDir/" transfer.xml");
    if (exists(transferDb))
        transfer.Load(transferDb);
    
    // update the subset from the transfer cache
    transfer.UpdateSubset(subset);
    transfer.Save(transferDb);
    
    // save the subset map
    subset.Save(subsetDb);

    // update the transfer cache from the subset
    subset.UpdateCache(transfer, setDir, transferDb);
}





