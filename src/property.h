//
//  property.h
//  distributed
//
//  Created by Brice Rivé on 11/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <string>

namespace nv {
    
#define Nv(T, s) \
class nv_##s##_nv: public nv::ValueBase {\
public:\
    typedef T Type;\
    nv_##s##_nv() {}\
    nv_##s##_nv(const T &val):mValue(val) {}\
    std::string Name()const {return ExtractName(typeid(this).name());}\
    operator T()const {return mValue;}\
    nv_##s##_nv &operator=(const T &val) {mValue=val; return *this;}\
    bool operator<(const nv_##s##_nv &rhs)const {return mValue<rhs.mValue;}\
    bool operator==(const nv_##s##_nv &rhs)const {return mValue==rhs.mValue;}\
    bool operator!=(const nv_##s##_nv &rhs)const {return !(mValue==rhs.mValue);}\
private:\
    T mValue;\
};\
nv_##s##_nv s;
    
#define NvString(s) Nv(std::string,s)
#define NvInt(s) Nv(int,s)
#define NvBool(s) Nv(bool,s)
    
    struct ValueBase
    {
        static std::string ExtractName(const std::string &typeName)
        {
            size_t posB=typeName.find("nv_");
            size_t posE=typeName.find("_nv");
            return typeName.substr(posB+3, posE-posB-3);
        }
    };
}

