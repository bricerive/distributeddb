//
//  utils.h
//  distributed
//
//  Created by Brice Rivé on 11/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <boost/filesystem.hpp>
#include <string>
#include <map>
#include <set>
#include <iostream>
#include "subset.h"
#include "transfer.h"


long GetFreeDiskSpace(const boost::filesystem::path &path);

boost::uintmax_t GetFolderSize(const boost::filesystem::path &path);

boost::filesystem::path HomeDir();
