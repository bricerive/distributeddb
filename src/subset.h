#pragma once
//
//  subset.h
//  distributed
//
//  Created by Brice Rivé on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include "property.h"
#include <string>
#include <map>
#include <set>
#include <typeinfo>
#include <boost/filesystem.hpp>
#include <tbb/blocked_range.h>

typedef boost::filesystem::path Path;

// store the subset data
//-------------------------
struct SubsetEntry {
    SubsetEntry(const std::string &location="", const std::string &name="", size_t size=0)
    : location(location), fileName(name), size(static_cast<int>(size)) {}
    NvString(location);
    NvString(fileName);
    NvInt(size);
    std::string RelativePath()const {return std::string(location)+"/"+std::string(fileName);}
    bool operator==(const SubsetEntry &rhs)const
    { return location==rhs.location && fileName==rhs.fileName && size==rhs.size; }
    bool operator<(const SubsetEntry &rhs)const
    {
        if (location!=rhs.location)
            return location<rhs.location;
        if (fileName!=rhs.fileName)
            return fileName<rhs.fileName;
        return size<rhs.size;
    }
};
typedef std::map<std::string, SubsetEntry> SubsetMap;

struct SubsetInfoNv {
    NvString(set);
    NvString(subset);
    NvInt(minSize);
    NvBool(updateFromOthers);
    NvBool(updateOthers);
    NvBool(forceRefreshCrcs);
};

class Transfer;

class Subset
{
public:
    Subset(const Path &path);
    void Save(const Path &xmlPath);
    void Load(const Path &xmlPath, bool skipInfo=false);
    void Refresh(const Path &xmlPath);
    void ReadInfo();
    void UpdateCache(Transfer &transfer, const Path &setDir, const Path &xmlPath);
    bool Wants(const std::string &crc);
    void AddEntry(const std::string &crc, const SubsetEntry &entry);
    
    bool AddEntry(const std::string &name, const std::string &crc, int size, const Path &src);

    
    Path GetPath()const {return mPath;}
    std::string Set()const {return mInfo.set;}
    std::string Name()const {return mInfo.subset;}
    
    typedef std::set<std::string> Unwanted;
    bool IsUnwanted(const std::string &crc)const;

private:
    // helpers
    //-------------------------
    struct FileInfo
    {
        FileInfo(const Path &path, size_t size): mPath(path), mSize(size) {}
        Path mPath;
        size_t mSize;
    };
    typedef std::vector<FileInfo> Files;

    class ComputeCrcs
    {
    public:
        void operator()( const tbb::blocked_range<size_t>& r ) const;
        ComputeCrcs(const Files &files, Subset &subset);
    private:
        const Files &mFiles;
        Subset &mSubset;
    };
    
    bool HasFile(const FileInfo &fileInfo);
    
    void UpdateCrcs(const Files &files);

    bool UpdateFromOthers()const {return mInfo.updateFromOthers;}
    bool UpdateOthers()const {return mInfo.updateOthers;}
    int MinSize()const {return mInfo.minSize;}

    Path RelativePath(const Path &absolutePath)const;

    Path mPath;
    SubsetInfoNv mInfo;
    SubsetMap mMap;
    Unwanted mUnwanted;
};
