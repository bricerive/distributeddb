################## What is it
The idea is to manage multiple instances of datasets.
Stuff like movies, books, software etc.
##################
Definition:
Set: a virtual dataset that is the reunion without duplicates of multiple subsets
Subset: an instance that is a sub-set of a specific set
Carrier: a vehicle that operates on one subset at a time
##################
Identification:
A set has a unique ID and a name
A subset has a unique ID, a name, and a owner set
##################
Contents:
-Carrier:
--Set: id, items
---Item info: id, name, crc
--Subset: id, items
---Item: id, location
-Subset: the subset data is in a file at the folder root (Subset.txt)
--Id: 
--Name:
##################
ToDo:
-Fix XML encoding of existing entries (e.g. " Readme")
\/-Detect duplicates in subset when refreshing
-Handling of duplicates
-Delete outdated entries in subset when refreshing
-Refresh of transfer dir
\/-Save subsets in alphabetical order
-Add deleted entries (CRC) to a subset so that we can indicate we don't want them anymore
-When loading subset, a file that is in the XML but not on disk sholl be considered as deleted -> add it to the deleted entries
-Limit Transfer taking account of disk space left