//
//  utils.cpp
//  distributed
//
//  Created by Brice Rivé on 11/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "utils.h"
#include "mylib/error.h"
#include <pwd.h>
#include <sys/mount.h>
#include <boost/iterator/transform_iterator.hpp>
#include <numeric>

using namespace boost::filesystem;
using namespace mylib;

long GetFreeDiskSpace(const boost::filesystem::path &path)
{
    const char *absoluteFilePath = path.c_str();
    struct statfs buf;
    if (statfs(absoluteFilePath, &buf)!=0)
        throw Err(_WHERE, "Cannot stat disk size");
    return buf.f_bfree*buf.f_bsize;
}

boost::uintmax_t GetFolderSize(const Path &path)
{
    typedef boost::uintmax_t (*Function)(const Path &);
    typedef boost::transform_iterator<Function, recursive_directory_iterator> doubling_iterator;
    doubling_iterator beg(recursive_directory_iterator(path), &boost::filesystem::file_size);
    doubling_iterator end(recursive_directory_iterator(), &boost::filesystem::file_size);
    return std::accumulate(beg, end, static_cast<boost::uintmax_t>(0));
}

path HomeDir()
{
    struct passwd *pw = getpwuid(getuid());
    
    const char *homedir = pw->pw_dir;
    return homedir;
}

