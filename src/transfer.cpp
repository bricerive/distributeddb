//
//  transfer.cpp
//  distributed
//
//  Created by Brice Rivé on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include "transfer.h"
#include "subset.h"
#include "tinyxml/tinyxml.h"
#include "mylib/error.h"
#include "mylib/log.h"
#include <boost/foreach.hpp>

using namespace mylib;
using namespace std;

Transfer::Transfer(const Path &transferDir)
    :mTransferDir(transferDir)
{}

TransferEntry &Transfer::GetEntry(const std::string &crc)
{
    return mMap[crc];
}

bool Transfer::HasEntry(const std::string &crc)const
{
    return mMap.find(crc)!=mMap.end();
}

void Transfer::Save(const Path &xmlPath)
{
    LogF(LOG_TRACE, "Transfer::Save(%s)", xmlPath.c_str());

    // build the TinyXml DOM
    TiXmlDocument doc;
    TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "", "" );  
	doc.LinkEndChild( decl ); 
    TiXmlElement * root = new TiXmlElement("ROOT");  
	doc.LinkEndChild( root );  
    BOOST_FOREACH(const TransferMap::value_type &v, mMap) {
        TiXmlElement *entry = new TiXmlElement(v.first.c_str());  
        root->LinkEndChild(entry);
        const TransferEntry &transferEntry(v.second);
        TiXmlElement *elt = new TiXmlElement("fileName");
        elt->LinkEndChild( new TiXmlText(transferEntry.mName.c_str()));  
        entry->LinkEndChild(elt);
        BOOST_FOREACH(const string &d, transferEntry.mDestinations) {
            elt = new TiXmlElement("destination");
            elt->LinkEndChild( new TiXmlText(d.c_str()));  
            entry->LinkEndChild(elt);
        }
    }
    doc.SaveFile(xmlPath.c_str());  
}

void Transfer::Load(const Path &xmlPath)
{
    LogF(LOG_TRACE, "Transfer::Load(%s)", xmlPath.c_str());

    TiXmlDocument doc(xmlPath.c_str());
	if (!doc.LoadFile()) throw Err(_WHERE, "Cannot load xml file [%s]", xmlPath.c_str());
    
    TiXmlElement* root = doc.FirstChildElement();
    for (TiXmlElement *pElem=root->FirstChildElement(); pElem; pElem=pElem->NextSiblingElement())
    {
        TransferEntry entry;
        const char *crc=pElem->Value();
        TiXmlElement *pElem2 = pElem->FirstChildElement("fileName");
        entry.mName = pElem2->GetText();
        pElem2 = pElem->FirstChildElement("destination");
        for (; pElem2; pElem2=pElem2->NextSiblingElement())
        {
            entry.mDestinations.insert(pElem2->GetText());
        }
        mMap[crc]=entry;
    }
}

// 2- Update the subset from the transfer cache
// -for each file in the transfer cache
// --if our subsetId says we were missing it
// ---if its crc is not in our map (we still don't have it)
// ----copy to relative path
// ----add it to our map
// ---remove our subset id
// --if there is no more subset id, remove from transfer cache
void Transfer::UpdateSubset(Subset &subset)
{
    LogF(LOG_TRACE, "Transfer::UpdateSubset(%s)", subset.GetPath().c_str());

    
    // for each entry in the transfer cache
    for (TransferMap::iterator i=mMap.begin(); i!=mMap.end(); )
    {
        const string &crc(i->first);
        TransferEntry &entry(i->second);
        TransferEntry::Destinations &destinations(entry.mDestinations);
        Path src = mTransferDir/entry.mName;
        
        // see if the subset is part of the destinations, meaning we were missing this
        // entry at some point
        TransferEntry::Destinations::iterator j = destinations.find(subset.Name());
        if (j!=destinations.end())
        {
            LogF(LOG_TRACE, "    Transfer [%s]", entry.mName.c_str());

            subset.AddEntry(entry.mName, crc, entry.mSize, src);
            
            // remove our subset from the destinations
            destinations.erase(j);
        }
        // if there is no more destination, remove it from the cache
        if (destinations.empty())
        {
            LogF(LOG_TRACE, "        Remove [%s]", entry.mName.c_str());
            remove(src);
            mMap.erase(i++); // invalidates i ?
        }
        else
        {
            ++i;
        }
    }
}
