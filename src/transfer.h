#pragma once
//
//  transfer.h
//  distributed
//
//  Created by Brice Rivé on 11/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <boost/filesystem.hpp>
#include <string>
#include <set>
#include <map>

// store the transfer directory data
//-------------------------
struct TransferEntry {
    std::string mName;
    int mSize;
    typedef std::set<std::string> Destinations;
    Destinations mDestinations;
};

class Subset;

class Transfer
{
public:
    typedef boost::filesystem::path Path;

    Transfer(const Path &transferDir);
    void Load(const Path &xmlPath);
    void Save(const Path &xmlPath);
    void UpdateSubset(Subset &subset);

    bool HasEntry(const std::string &crc)const;
    TransferEntry &GetEntry(const std::string &crc);
    const Path &GetDir()const {return mTransferDir;}
    
private:
    Path mTransferDir;
    typedef std::map<std::string, TransferEntry> TransferMap;
    TransferMap mMap;
};
