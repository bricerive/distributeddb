# About distributeddb #

A distributed data set system

### What is it? ###

* The idea is to manage a data set (stuff like movies, books, music, pictures, software etc.) that is stored across multiple machines/disks.
* Definitions:
    * Set: a virtual dataset that is the reunion without duplicates of multiple subsets
    * Subset: an instance that is a sub-set of a specific set
    * Carrier: a vehicle that operates on one subset at a time
*Identification:
    * A set has a unique ID and a name
    * A subset has a unique ID, a name, and a owner set
* Version 0.0.0

### How do I get set up? ###

* How to build
    * Grab the depot
    * Run CMake - You need these libraries
        * [MyLib}(https://bitbucket.org/bricerive/mylib)
        * boost::system
        * boost::filesystem
        * tbb
    * Build with XCode
* Todo
    * Build it on other platforms


### Who do I talk to? ###

* Brice Rivé
